create database if not exists books;
use books;
create table books(
	booksID int(12) auto_increment primary key,
	title varchar(200),
	description varchar(2000),
	price double(6,2)
);

insert into books(title,description,price) values 
("Java programiranje","Uvod u Java programiranje",85.99),
("NoSQL baze","Uvod u MongoDB",85.99),
("Java programiranje","Uvod u Java programiranje",95.99),
("Programiranje za pocetnike","Osnove programiranjea",155.99);