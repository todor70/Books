/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Books;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author UNDP
 */
public class BooksDAOImpl implements BooksDAO {

    Session session = null;

    @Override
    public List<Books> search(String search) {
        session = util.HibernateUtil.getSessionFactory().openSession();
        List<Books> lista = session.createCriteria(Books.class).add(Restrictions.or(Restrictions.like("title", "%" + search + "%"), (Restrictions.like("description", "%" + search + "%")))).list();

        return lista;
    }

}
